const winston = require('winston');
const { Postgres } = require('winston-postgres');
const { S3StreamLogger } = require('s3-streamlogger');
const _ = require('lodash');

const createTransports = (config) => {
  const appName = config.appName || 'unknownApp';
  const transports = [];
  if (hasConsoleConfig(config.console)) {
    transports.push(createConsoleTransport(appName, config.console));
  }
  if (hasFileConfig(config.file)) {
    transports.push(createFileTransport(appName, config.file));
  }
  if (hasS3Config(config.s3)) {
    transports.push(createS3Transport(appName, config.s3));
  }
  if (hasPostgresConfig(config.postgres)) {
    transports.push(createPostgresTransport(appName, config.postgres));
  }
  return transports;
};

const hasConsoleConfig = config => true;
const createConsoleTransport = (__, config) =>
  new winston.transports.Console(config);

const hasS3Config = config =>
  _.has(config, 'key') &&
  _.has(config, 'secret') &&
  _.isString(config.key) &&
  _.isString(config.secret);
const createS3Transport = (appName, config) => {
  const s3Stream = new S3StreamLogger({
    bucket: 'elliptic-apps-logs',
    access_key_id: config.key,
    secret_access_key: config.secret,
    name_format: `%Y-%m-%d-%H-%M-${appName}.log`,
  });
  return new winston.transports.File({
    level: config.level,
    stream: s3Stream,
  });
};

const hasFileConfig = config =>
  _.has(config, 'filename') && _.isString(config.filename);
const createFileTransport = (__, config) => new winston.transports.File(config);

const hasPostgresConfig = config =>
  _.has(config, 'connectionString') && _.isString(config.connectionString);
const createPostgresTransport = (__, config) => new Postgres(config);

module.exports = createTransports;
