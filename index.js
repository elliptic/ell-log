const cfg = require('./config');
const winston = require('winston');
const _ = require('lodash');
const createTransports = require('./transports');

const rewriteAuthorization = (level, msg, meta) => {
  if (meta.req && meta.req.headers) {
    if (meta.req.headers.authorization !== undefined) {
      meta.req.headers.authorization = '<HIDDEN>';
    }
    if (meta.req.headers.cookie !== undefined) {
      meta.req.headers.cookie = '<HIDDEN>';
    }
  }
  return meta;
};

const logger = (config = {}) => {
  const level = _.defaultTo(config.level, cfg.level);
  const levels = _.defaultTo(config.levels, cfg.levels);
  const colors = _.defaultTo(config.colors, cfg.colors);
  const transportConfig = _.merge(cfg.transports, config.transports);
  const transports = createTransports(transportConfig);

  return new winston.Logger({
    level,
    levels,
    colors,
    transports,
    rewriters: [rewriteAuthorization],
  });
};

module.exports = logger;
