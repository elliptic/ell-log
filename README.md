Ell-log
======

A simple wrapper for winston (with sensible defaults) for use in an express app (or elsewhere).

Quick start
-----

Run `npm install ell-log --save` for your app.

Then:

```js
const logger = require('ell-log');
const config = {
  appName: 'foobar',
  level: 'info',
  file: {
    level: 'error',
    filename: 'foobar.log',
  },
};
logger(config);

logger.info('This is a message');
```
